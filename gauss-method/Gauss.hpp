//
// Created by wlaszlo on 4/7/18.
//

#ifndef GAUSS_METHOD_GAUSS_HPP
#define GAUSS_METHOD_GAUSS_HPP

#include <vector>
#include <string>
#include <cmath>
#include "Output.hpp"

class Gauss {
    std::vector<std::vector<double>> matrix{};
    std::vector<double> solution{};
    std::vector<std::string> helpX{};
    std::array<int, 1> colSwaps{};
    Output out;
    bool runningWithSemiPivot = false;
    int n, colSwapIndex = -1;

    void buildTriangleMatrix() {
        for (int k = 0; k < n - 1; k++) {
            if (matrix[k][k] == 0) {
                if (runningWithSemiPivot) {
                    swapLines(k, getNextNenulIndex(k, 0));
                } else {
                    auto maxIndex = getMaxValueIndex(k);

                    if (k != maxIndex[0]) {
                        swapLines(k, maxIndex[0]);
                    }
                    if (k != maxIndex[1]) {
                        swapColumns(k, maxIndex[1]);
                        colSwaps[++colSwapIndex] = k;
                        colSwaps[++colSwapIndex] = maxIndex[1];
                    }
                }

                out.print(matrix);
            }

            for (int i = k + 1; i < n; i++) {
                double m = -(matrix[i][k] / matrix[k][k]);
                std::cout << "L" << i + 1 << " -> L" << i + 1 << " + (" + to_string(m) + " * L" << k + 1 << ") \n";

                for (int j = k; j < n + 1; j++) {
                    matrix[i][j] = matrix[i][j] + m * matrix[k][j];
                }
            }
            out.print(matrix);
        }
    }

    int getNextNenulIndex(int line, int column) {
        int found = 0, i = line + 1;
        while (!found && i < n) {
            if (matrix[i][column] != 0) {
                found = 1;
            } else {
                i++;
            }
        }

        return i;
    }

    void swapLines(int ln1, int ln2) {
        std::cout << "L" << ln1 + 1 << " <-> L" << ln2 + 1 << "\n";

        for (int j = 0; j < n + 1; j++) {
            double aux = matrix[ln1][j];
            matrix[ln1][j] = matrix[ln2][j];
            matrix[ln2][j] = aux;
        }
    }

    void swapColumns(int col1, int col2) {
        std::cout << "C" << col1 + 1 << " <-> C" << col2 + 1 << "\n";

        auto auxH = helpX[col1];
        helpX[col1] = helpX[col2];
        helpX[col2] = auxH;

        for (int i = 0; i < n; i++) {
            double aux = matrix[i][col1];
            matrix[i][col1] = matrix[i][col2];
            matrix[i][col2] = aux;
        }
    }

    std::array<int, 2> getMaxValueIndex(int k) {
        std::array<int, 2> maxIndex({k, k});

        for (int i = k; i < n; i++) {
            for (int j = k; j < n; j++) {
                if (matrix[i][j] > matrix[maxIndex[0]][maxIndex[1]]) {
                    maxIndex[0] = i; //line
                    maxIndex[1] = j; //column
                }
            }
        }

        return maxIndex;
    }

    void createHelpVector(std::vector<std::string> &t) {
        char ch = 'a';

        for (int i = 0; i < matrix.size(); i++) {
            std::string cha(1, ch++);
            t.push_back(cha);
        }
    }

    std::string to_string(const double value) {
        std::ostringstream out;
        double intpart;
        double fractpart = modf(value, &intpart);

        if (fractpart == 0) {
            out << std::fixed << std::setprecision(0) << value;
        } else {
            out << std::fixed << std::setprecision(2) << value;
        }
        return out.str();
    }

    void buildSolution() {
        solution.resize(static_cast<unsigned long>(n));

        solution[n - 1] = matrix[n - 1][n] / matrix[n - 1][n - 1];
        std::cout << helpX[n - 1] << "=" + to_string(matrix[n - 1][n]) + "/" + to_string(matrix[n - 1][n - 1]) << " => ";
        std::cout << helpX[n - 1] << "=" + to_string(solution[n - 1]) << "\n";

        for (int i = n - 2; i >= 0; i--) {
            std::string message = helpX[i] + "=";
            std::string sum1{}, sum2{};
            double sum = 0;

            for (int j = i + 1; j < n; j++) {
                sum += matrix[i][j] * solution[j];
                sum1 += "(" + to_string(matrix[i][j]) + "*" + to_string(solution[j]) + ")+";
                sum2 += "(" + to_string(matrix[i][j] * solution[j]) + ")+";
            }
            solution[i] = (matrix[i][n] - sum) / matrix[i][i];

            sum1.pop_back();
            sum2.pop_back();
            std::cout << message << "[" << to_string(matrix[i][n]) << "-" << sum1 << "]/" << to_string(matrix[i][i])
                      << " => ";
            std::cout << message << to_string(matrix[i][n] - sum) << "/" << to_string(matrix[i][i]) << " => ";
            std::cout << message << to_string(solution[i]) << "\n";
        }
    }

    void getRealSolution() {
        if (colSwapIndex != -1) {
            for (int i = 0; i < colSwapIndex + 1; i += 2) {
                swapFreeValues(colSwaps[i], colSwaps[i + 1]);
            }
        }
    }

    void swapFreeValues(int ln1, int ln2) {
        double aux = solution[ln1];
        solution[ln1] = solution[ln2];
        solution[ln2] = aux;
    }

    void run() {
        std::cout<< "A*x=b, condiție: să existe det A != 0 ⇔ x = A^(-1) * b\n\n";

        out.print(matrix);
        buildTriangleMatrix();

        buildSolution();
        getRealSolution();

        std::cout << "\nSolution is: ";
        out.print(solution);
    }

public:

    explicit Gauss(const std::vector<std::vector<double>> &matrix) : matrix(matrix) {
        n = static_cast<int>(matrix.size());
        createHelpVector(helpX);
    }

    void runWithSemiPivot() {
        runningWithSemiPivot = true;
        run();
    }

    void runWithTotalPivot() {
        runningWithSemiPivot = false;
        run();
    }
};

#endif //GAUSS_METHOD_GAUSS_HPP
