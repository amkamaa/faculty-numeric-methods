#include <iostream>
#include "Cholesky.hpp"

int main() {

    Cholesky cholesky({{3,1,1},
                       {1,3,2},
                       {1,2,7}}, {6,8,17});

    cholesky.run();
    return 0;
}