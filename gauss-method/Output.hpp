//
// Created by wlaszlo on 4/7/18.
//

#ifndef GAUSS_METHOD_OUTPUT_HPP
#define GAUSS_METHOD_OUTPUT_HPP

#include <iostream>
#include <vector>
#include <sstream>
#include <iomanip>

class Output {
public:
    Output() = default;
private:
    std::string to_string(const double value) {
        std::ostringstream out;
        double intpart;
        double fractpart = modf(value, &intpart);

        if (fractpart == 0) {
            out << std::fixed << std::setprecision(0) << value;
        } else {
            out << std::fixed << std::setprecision(2) << value;
        }
        return out.str();
    }

public:
    void print(const std::vector<std::vector<std::string>> &matrix) {
        for (const auto &line : matrix) {
            for (const auto &element : line) {
                std::cout << element << " ";
            }
            std::cout << "\n";
        }
    }

    template<class T>
    void print(std::vector<T> vector) {
        std::stringstream buffer{};

        for (auto it : vector) {
            buffer << to_string(it) << ", ";
        }
        std::cout << buffer.str().substr(0, buffer.str().size() - 2);
    }

    void print(std::vector<std::vector<double>> matrix) {
        for (const auto &line : matrix) {
            for (const auto &element : line) {
                std::cout << std::setw(7) << to_string(element) << " ";
            }
            std::cout << "\n";
        }
        std::cout << "\n";
    }

    template<class T, class U>
    void print(std::vector<std::vector<T>> matrix1, std::vector<std::vector<T>> matrix2,
               std::vector<std::vector<U>> result, const std::string &operation, const std::string &description) {
        bool operationPrinted1 = false;
        bool operationPrinted2 = false;
        int space = 5;

        std::cout << "+---> " << description << "\n";

        for (int i = 0; i < matrix1.size(); i++) {
            std::cout << "|";
            for (int j = 0; j < matrix1.size(); j++) {
                std::cout << std::setw(space) << matrix1[i][j];

                if (i == 0 && j + 1 == matrix1.size() && !operationPrinted1) {
                    std::cout << std::setw(2) << "|" << std::setw(space) << operation;
                    operationPrinted1 = true;
                } else if (j + 1 == matrix1.size()) {
                    std::cout << std::setw(2) << "|";
                }

            }

            for (int j = 0; j < matrix1.size(); j++) {
                if (i != 0 && j == 0) {
                    std::cout << std::setw(space * 2) << "|" << std::setw(space) << std::setprecision(2)
                              << matrix2[i][j];

                } else if (i == 0 && j == 0) {
                    std::cout << std::setw(space) << " |" << std::setw(space) << std::setprecision(2) << matrix2[i][j];
                } else {
                    std::cout << std::setw(space) << std::setprecision(2) << matrix2[i][j];
                }

                if (i == 0 && j + 1 == matrix1.size() && !operationPrinted2) {
                    std::cout << std::setw(2) << "|" << std::setw(space) << "=";
                    operationPrinted2 = true;
                } else if (j + 1 == matrix1.size()) {
                    std::cout << std::setw(2) << "|";
                }

            }

            for (int j = 0; j < matrix1.size(); j++) {
                if (i != 0 && j == 0) {
                    std::cout << std::setw(space * 2) << "|" << std::setw(space) << std::setprecision(2)
                              << result[i][j];

                } else if (i == 0 && j == 0) {
                    std::cout << std::setw(space) << " |" << std::setw(space) << std::setprecision(2) << result[i][j];
                } else {
                    std::cout << std::setw(space) << std::setprecision(2) << result[i][j];
                }

                if (j + 1 == matrix1.size()) {
                    std::cout << std::setw(2) << "|";
                }
            }

            std::cout << "\n";
        }
    }

    template<class T>
    void print(std::vector<std::vector<T>> matrix1, std::vector<std::vector<T>> matrix2,
               const std::string &description) {
        int space = 5;
        bool operationPrinted = false;

        std::cout << "+---> " << description << "\n";

        for (int i = 0; i < matrix1.size(); i++) {
            std::cout << "|";
            for (int j = 0; j < matrix1.size(); j++) {
                std::cout << std::setw(space) << std::setprecision(2) << matrix1[i][j];

                if (i == 0 && j + 1 == matrix1.size() && !operationPrinted) {
                    std::cout << std::setw(2) << "|" << std::setw(space) << "&";
                    operationPrinted = true;
                } else if (j + 1 == matrix1.size()) {
                    std::cout << std::setw(2) << "|";
                }

            }

            for (int j = 0; j < matrix1.size(); j++) {
                if (i != 0 && j == 0) {
                    std::cout << std::setw(space * 2) << "|" << std::setw(space) << std::setprecision(2)
                              << matrix2[i][j];

                } else if (i == 0 && j == 0) {
                    std::cout << std::setw(space) << " |" << std::setw(space) << std::setprecision(2) << matrix2[i][j];
                } else {
                    std::cout << std::setw(space) << std::setprecision(2) << matrix2[i][j];
                }

                if (i == 0 && j + 1 == matrix1.size()) {
                    std::cout << std::setw(2) << "|";
                } else if (j + 1 == matrix1.size()) {
                    std::cout << std::setw(2) << "|";
                }

            }
            std::cout << "\n";
        }
    }

    template<class T, class U, class K>
    void print(std::vector<std::vector<T>> matrix, std::vector<U> vector,
               std::vector<K> result, const std::string &operation, const std::string &description) {
        bool operationPrinted = false;
        int space = 5;

        std::cout << "+---> " << description << "\n";

        for (int i = 0; i < matrix.size(); i++) {
            std::cout << "|";
            for (int j = 0; j < matrix.size(); j++) {
                std::cout << std::setw(space) << matrix[i][j];

                if (i == 0 && j + 1 == matrix.size() && !operationPrinted) {
                    std::cout << std::setw(2) << "|" << std::setw(space) << operation;
                    operationPrinted = true;
                } else if (j + 1 == matrix.size()) {
                    std::cout << std::setw(2) << "|";
                }

            }

            if (i == 0) {
                std::cout << std::setw(space) << " |" << std::setw(space) << std::setprecision(2) << vector[i] << " |"
                          << std::setw(space) << "=";
            } else {
                std::cout << std::setw(space * 2) << "|" << std::setw(space) << std::setprecision(2)
                          << vector[i] << " |";
            }

            if (i == 0) {
                std::cout << std::setw(space) << " |" << std::setw(space) << std::setprecision(2) << result[i] << " |";
            } else {
                std::cout << std::setw(space * 2) << "|" << std::setw(space) << std::setprecision(2)
                          << result[i] << " |";
            }

            std::cout << "\n";
        }
    }
};

#endif //GAUSS_METHOD_OUTPUT_HPP
