#include <iostream>
#include "LuFactorization.hpp"

int main() {
    LuFactorization lu({{3,1,1},
                        {1,3,2},
                        {1,2,7}}, {6,8,17});

    lu.runDoolittle();
//    lu.runCrout();
    return 0;
}