//
// Created by wlaszlo on 4/6/18.
//

#ifndef NUMERIC_METHODS_CHOLESKY_HPP
#define NUMERIC_METHODS_CHOLESKY_HPP

#include <vector>
#include <iostream>
#include <iomanip>
#include <cmath>
#include "Output.hpp"

class Cholesky {
private:
    int n;
    std::vector<std::vector<double>> matrix{}, L{}, tL{};
    std::vector<double> X{}, b{}, Y{};
    std::vector<std::string> helpX{}, helpY{};
    std::vector<std::vector<std::string>> helpL{}, helptL{};
    Output out;

    auto getTranspose(const std::vector<std::vector<double>> matrix) {
        std::vector<std::vector<double>> transpose;
        createMatrix(transpose);

        for (int i = 0; i < matrix.size(); i++) {
            for (int j = i; j < matrix[i].size(); j++) {
                transpose[i][j] = matrix[j][i];
            }
        }

        return transpose;
    }

    void createMatrix(std::vector<std::vector<double>> &t) {
        for (int i = 0; i < matrix.size(); i++) {
            std::vector<double> line;

            for (int j = 0; j < matrix.size(); j++) {
                line.push_back(0);
            }
            t.push_back(line);
        }
    }

    void createHelpVector(std::vector<std::string> &t, std::string ch) {
        for (int i = 0; i < matrix.size(); i++) {
            t.push_back(ch + std::to_string(i + 1));
        }
    }

    void createVector(std::vector<double> &t) {
        for (int i = 0; i < matrix.size(); i++) {
            t.push_back(0);
        }
    }

    std::string to_string(const double value) {
        std::ostringstream out;
        double intpart;
        double fractpart = modf(value, &intpart);

        if (fractpart == 0) {
            out << std::fixed << std::setprecision(0) << value;
        } else {
            out << std::fixed << std::setprecision(2) << value;
        }
        return out.str();
    }

    void buildHelperVector(std::vector<std::string>& vector, std::string ch) {
        for (int i=0; i<n; i++) {
            vector.push_back(ch + std::to_string((i+1)));
        }
    }

    bool isSymetricMatrix() {
        bool ok = true;
        int i = 0, j = 0;

        while (i < matrix.size() && ok) {
            while (j < i && ok) {
                if (matrix[i][j] != matrix[j][i]) {
                    ok = false;
                }
                j++;
            }
            i++;
        }

        return ok;
    }

    bool isPositiveDefined() {
        bool ok = true;
        int i = 0, j = 0;

        while (i < matrix.size() && ok) {
            double sum = 0;

            for (int j = 0; j < matrix[i].size(); j++) {
                if (i != j) {
                    sum += matrix[i][j];
                }
            }

            if (matrix[i][i] <= 0 || matrix[i][i] < sum) {
                ok = false;
            }

            i++;
        }

        return ok;
    }

    void positiveDefinedProof() {
        bool ok = true;
        int i = 0;

        while (i < matrix.size() && ok) {
            double sum = 0;
            std::string message{};
            std::string sign{};

            if (matrix[i][i] <= 0) {
                ok = false;
                sign = "<=";
                std::cout << to_string(matrix[i][i]) + " <= 0; ";
            } else {
                std::cout << to_string(matrix[i][i]) + " > 0; ";
                sign = ">=";

                for (int j = 0; j < matrix[i].size(); j++) {
                    if (i != j) {
                        sum += matrix[i][j];
                        message += to_string(matrix[i][j]) + "+";
                    }
                }
            }

            if (matrix[i][i] < sum) {
                sign = "!>=";
                ok = false;
            }

            message.pop_back();
            message += "; " + to_string(matrix[i][i]) + " " + sign + " " + to_string(sum);
            message = to_string(matrix[i][i]) + " " + sign + " " + message;
            std::cout << message << "\n";

            i++;
        }
        std::cout << "\n";
    }

    void buildHelpL() {
        for (int i = 0; i < matrix.size(); i++) {
            std::vector<std::string> line;

            for (int j = 0; j < matrix.size(); j++) {
                line.push_back(i >= j ? ("L" + std::to_string(i + 1) + std::to_string(j + 1)) : "0");
            }
            helpL.push_back(line);
        }
    }

    void buildHelptL() {
        for (int i = 0; i < matrix.size(); i++) {
            std::vector<std::string> line;

            for (int j = 0; j < matrix.size(); j++) {
                line.push_back(i <= j ? ("L" + std::to_string(i + 1) + std::to_string(j + 1)) : "0");
            }
            helptL.push_back(line);
        }
    }

    void buildHelpY() {
        buildHelperVector(helpY, "y");
    }

    void buildHelpX() {
        buildHelperVector(helpX, "x");
    }

    void buildLMatrix() {
        createMatrix(L);
        L[0][0] = sqrt(matrix[0][0]);

        for (int i = 0; i < matrix.size(); i++) {
            for (int j = 0; j <= i; j++) {
                double sum = 0;
                std::string message = "";

                std::cout << "l" << i + 1 << "*c" << j + 1 << " => ";
                if (i == j) {
                    for (int k = 0; k <= i; k++) {
                        if (k < i) {
                            sum += pow(L[i][k], 2);
                        }
                        message += helpL[i][k] + "^2+";
                    }

                    L[i][i] = sqrt(matrix[i][i] - sum);
                    message.pop_back();
                    message += "=" + to_string(matrix[i][i]);
                    message += " => " + helpL[i][i] + "=" + to_string(sqrt(matrix[i][i] - sum));

                    std::cout << message;

                } else {
                    for (int k = 0; k <= j; k++) {
                        if (k < j) {
                            sum += L[i][k] * L[j][k];
                        }
                        message += helpL[j][k] + "*" + helpL[i][k] + "+";
                    }

                    L[i][j] = (matrix[i][j] - sum) / L[j][j];

                    message.pop_back();
                    message += "=" + to_string(matrix[i][j]);
                    message += " => " + helpL[i][j] + "=" + to_string((matrix[i][j] - sum) / L[j][j]);
                    std::cout << message;
                }
                std::cout << "\n";
            }
        }
        std::cout << "\n";
    }

    void printAMatrix() {
        std::cout << "Matrix A: \n";
        out.print(matrix);
    }

    void printTransposeAMatrix() {
        std::cout << "Matrix transpose A: \n";
        out.print(getTranspose(matrix));
    }

    void printTransposeLMatrix() {
        std::cout << "Matrix transpose L: \n";
        out.print(tL);
    }

    void printLMatrix() {
        std::cout << "Matrix L: \n";
        out.print(L);
    }

    void printY() {
        std::cout << "Y = (";
        out.print(Y);
        std::cout << ")\n";
    }

    void printX() {
        std::cout << "X = (";
        out.print(X);
        std::cout << ")\n";
    }

    void buildTransposeTMatrix() {
        tL = getTranspose(L);
    }

    void buildSteps() {
        buildHelpL();
        buildHelptL();

        out.print(helpL, helptL, matrix, "*", "Matrix L*tL=A");
        std::cout << "\n";
    }

    void buildY() {
        createVector(Y);
        createHelpVector(helpY, "y");
        std::string message{};

        Y[0] = b[0] / L[0][0];
        message += helpY[0] + "=" + to_string(b[0]) + "/" + helpL[0][0] + " => ";
        message += helpY[0] + "=" + to_string(b[0]) + "/" + to_string(L[0][0]) + " => ";
        message += helpY[0] + "=" + to_string(Y[0]) + "\n";
        std::cout << message;

        for (int i = 1; i < matrix.size(); i++) {
            message = "";
            std::string sum1{}, sum2{};
            double sum = 0;

            for (int j = 0; j <= i; j++) {
                sum1 += helpL[i][j] + "*" + helpY[j] + "+";
                sum2 += to_string(L[i][j] * Y[j]) + "+";
                sum += L[i][j] * Y[j];
            }

            Y[i] = (b[i] - sum) / L[i][i];

            sum1.pop_back();
            sum2.pop_back();
            message += helpY[i] + "=(" + sum1 + ")/" + helpL[i][i] + " => ";
            message += helpY[i] + "=(" + sum2 + ")/" + to_string(L[i][i]) + " => ";
            message += helpY[i] + "=" + to_string(Y[i]) + "\n";
            std::cout << message;
        }
    }

    void buildX() {
        createVector(X);
        createHelpVector(helpX, "x");
        X[n - 1] = Y[n - 1] / tL[n - 1][n - 1];
        std::string message{};

        message += helpX[n - 1] + "=" + to_string(Y[n - 1]) + "/" + helptL[n - 1][n - 1] + " => ";
        message += helpX[n - 1] + "=" + to_string(Y[n - 1]) + "/" + to_string(tL[n - 1][n - 1]) + " => ";
        message += helpX[n - 1] + "=" + to_string(X[n - 1]) + "\n";
        std::cout << message;

        for (int i = n - 2; i >= 0; i--) {
            message = "";
            std::string sum1{}, sum2{};
            double sum = 0;

            for (int j = i + 1; j < n; j++) {
                sum += tL[i][j] * X[j];
                sum1 += helptL[i][j] + "*" + helpX[j] + "+";
                sum2 += to_string(tL[i][j] * X[j]) + "+";
            }

            X[i] = (Y[i] - sum) / tL[i][i];

            sum1.pop_back();
            sum2.pop_back();
            message += helpX[i] + "=(" + sum1 + ")/" + helptL[i][i] + " => ";
            message += helpX[i] + "=(" + sum2 + ")/" + to_string(tL[i][i]) + " => ";
            message += helpX[i] + "=" + to_string(X[i]) + "\n";
            std::cout << message;
        }
    }

    void printSymmetryProof() {
        out.print(matrix, getTranspose(matrix), "A and tA");
    }

    void printLMatrixTuple() {
        out.print(L, tL, "T and tT");
    }

    void printLYb() {
        out.print(L, helpY, b, "*", "Matrix L*y=b");
    }

    void printtLXY() {
        out.print(tL, helpX, Y, "*", "Matrix tL*x=y");
    }

public:
    Cholesky(std::vector<std::vector<double>> matrix, std::vector<double> b) {
        this->matrix = matrix;
        this->b = b;
        this->n = static_cast<int>(matrix.size());
    }

    void run() {
        std::cout << "Se poate aplica Cholesky?\n\n";
        if (!isSymetricMatrix()) {
            std::cout << "a) Matricea A nu este simetrica!\n";
            exit(0);
        }

        std::cout << "a) Matricea este simetrica! A = tA\n";
        printSymmetryProof();
        std::cout<<"\n";

        if (!isPositiveDefined()) {
            std::cout << "b) Matricea A nu este pozitiv definita!\n";
            positiveDefinedProof();
            exit(0);
        }

        std::cout << "b) Matricea este pozitiv definita (elementele de pe d.p. > 0 si e diagonal dominanta!\n";
        positiveDefinedProof();

        buildSteps();

        buildLMatrix();
        buildTransposeTMatrix();
        printLMatrixTuple();

        std::cout<<"\n";
        std::cout<<"L*y=b   => y\n";
        std::cout<<"tL*x=y  => x\n\n";

        buildHelpY();
        printLYb();
        std::cout<<"\n";

        buildY();
        printY();

        std::cout<<"\n";
        buildHelpX();
        printtLXY();
        std::cout<<"\n";

        buildX();
        printX();
    }
};


#endif //NUMERIC_METHODS_CHOLESKY_HPP
