//
// Created by William on 10.04.2018.
//

#ifndef LU_LUFACTORIZATION_HPP
#define LU_LUFACTORIZATION_HPP

#include <utility>
#include <iostream>
#include "Output.h"

class LuFactorization {
private:
    std::vector<std::vector<double>> matrix{}, L{}, U{};
    std::vector<std::vector<std::string>> helpL{}, helpU{};
    std::vector<double> X{}, Y{}, b{};
    std::vector<std::string> helpX{}, helpY{};
    bool doolittle = false;
    Output out;
    int n;

    void buildLUDoolittle() {
        for (int i = 0; i < n; i++) {
            L[i][i] = 1;
        }

        std::string buffer1{}, buffer2{};
        for (int i = 0; i < n; i++) {

            for (int k = i; k < n; k++) {
                double sum = 0;
                std::string sum1 = "", sum2 = "", sum3 = "";

                for (int j = 0; j < i; j++) {
                    sum += (L[i][j] * U[j][k]);
                    sum1 += "(" + helpL[i][j] + "*" + helpU[j][k] + ")+";
                    sum2 += "(" + out.to_string(L[i][j]) + "*" + out.to_string(U[j][k]) + ")+";
                    sum3 += "" + out.to_string(L[i][j] * U[j][k]) + "+";
                }

                U[i][k] = matrix[i][k] - sum;

                sum1 = out.removeLastChar(sum1);
                sum2 = out.removeLastChar(sum2);
                sum3 = out.removeLastChar(sum3);

                buffer1 += helpU[i][k] + "=" + out.to_string(matrix[i][k]);
                buffer1 += (sum1.empty() ? "" : ("-" + sum1));
                buffer1 += (sum2.empty() ? "" : ("=" + out.to_string(matrix[i][k]) + "-" + sum2));
                buffer1 += (sum3.empty() ? "" : ("=" + out.to_string(matrix[i][k]) + "-(" + sum3 + ")"));
                buffer1 += (i == 0 ? "" : ("=" + out.to_string(U[i][k]))) + "\n";
            }

            buffer1 += out.to_string(U);

            for (int k = i + 1; k < n; k++) {
                double sum = 0;
                std::string sum1 = "", sum2 = "", sum3 = "";

                for (int j = 0; j < i; j++) {
                    sum += (L[k][j] * U[j][i]);
                    sum1 += "(" + helpL[k][j] + "*" + helpU[j][i] + ")+";
                    sum2 += "(" + out.to_string(L[k][j]) + "*" + out.to_string(U[j][i]) + ")+";
                    sum3 += "" + out.to_string(L[k][j] * U[j][i]) + "+";
                }

                L[k][i] = (matrix[k][i] - sum) / U[i][i];

                sum1 = out.removeLastChar(sum1);
                sum2 = out.removeLastChar(sum2);
                sum3 = out.removeLastChar(sum3);

                buffer2 += helpL[k][i] + "=(" + out.to_string(matrix[k][i]) + (sum1.empty() ? "" : "-" + sum1) + ")/" +
                           helpU[i][i];
                buffer2 += "=(" + out.to_string(matrix[k][i]) + (sum2.empty() ? "" : "-" + sum2) + ")/" +
                           out.to_string(U[i][i]);
                buffer2 += "=(" + out.to_string(matrix[k][i]) + (sum3.empty() ? "" : "-" + sum3) + ")/" +
                           out.to_string(U[i][i]);
                buffer2 += "=" + out.to_string(L[k][i]) + "\n";
            }

            buffer2 += out.to_string(L);
        }
        std::cout << buffer1 << "\n";
        std::cout << buffer2 << "\n";
    }

    void buildLUCrout() {
        for (int i = 0; i < n; i++) {
            U[i][i] = 1;
        }

        std::string buffer1{}, buffer2{};
        for (int i = 0; i < n; i++) {

            for (int k = i; k < n; k++) {
                double sum = 0;
                std::string sum1 = "", sum2 = "", sum3 = "";

                for (int j = 0; j < i; j++) {
                    sum += (L[k][j] * U[j][i]);
                    sum1 += "(" + helpL[k][j] + "*" + helpU[j][i] + ")+";
                    sum2 += "(" + out.to_string(L[k][j]) + "*" + out.to_string(U[j][i]) + ")+";
                    sum3 += "" + out.to_string(L[k][j] * U[j][i]) + "+";
                }

                L[k][i] = matrix[k][i] - sum;

                sum1 = out.removeLastChar(sum1);
                sum2 = out.removeLastChar(sum2);
                sum3 = out.removeLastChar(sum3);

                buffer1 += helpL[k][i] + "=" + out.to_string(matrix[k][i]);
                buffer1 += (sum1.empty() ? "" : ("-" + sum1));
                buffer1 += (sum2.empty() ? "" : ("=" + out.to_string(matrix[k][i]) + "-" + sum2));
                buffer1 += (sum3.empty() ? "" : ("=" + out.to_string(matrix[k][i]) + "-(" + sum3 + ")"));
                buffer1 += (i == 0 ? "" : ("=" + out.to_string(L[k][i]))) + "\n";

            }

            buffer1 += out.to_string(L);

            if (i < n - 1) {
                for (int k = i + 1; k < n; k++) {
                    double sum = 0;
                    std::string sum1 = "", sum2 = "", sum3 = "";

                    for (int j = 0; j < i; j++) {
                        sum += (L[i][j] * U[j][k]);
                        sum1 += "(" + helpL[i][j] + "*" + helpU[j][k] + ")+";
                        sum2 += "(" + out.to_string(L[i][j]) + "*" + out.to_string(U[j][k]) + ")+";
                        sum3 += "" + out.to_string(L[i][j] * U[j][k]) + "+";
                    }

                    U[i][k] = (matrix[i][k] - sum) / L[i][i];

                    sum1 = out.removeLastChar(sum1);
                    sum2 = out.removeLastChar(sum2);
                    sum3 = out.removeLastChar(sum3);

                    buffer2 +=
                            helpU[i][k] + "=(" + out.to_string(matrix[i][k]) + (sum1.empty() ? "" : "-" + sum1) + ")/" +
                            helpL[i][i];
                    buffer2 += "=(" + out.to_string(matrix[i][k]) + (sum2.empty() ? "" : "-" + sum2) + ")/" +
                               out.to_string(L[i][i]);
                    buffer2 += "=(" + out.to_string(matrix[i][k]) + (sum3.empty() ? "" : "-" + sum3) + ")/" +
                               out.to_string(L[i][i]);
                    buffer2 += "=" + out.to_string(U[i][k]) + "\n";
                }

                buffer2 += out.to_string(U);
            }
        }

        std::cout << buffer1 << "\n";
        std::cout << buffer2 << "\n";
    }

    void buildHelpLU() {
        for (int i = 0; i < matrix.size(); i++) {
            std::vector<std::string> lineL;
            std::vector<std::string> lineU;

            for (int j = 0; j < matrix.size(); j++) {
                if (doolittle) {
                    lineL.push_back(i == j ? "1" : i > j ? ("L" + std::to_string(i + 1) + std::to_string(j + 1)) : "0");
                    lineU.push_back(i <= j ? ("U" + std::to_string(i + 1) + std::to_string(j + 1)) : "0");
                } else {
                    lineL.push_back(i >= j ? ("L" + std::to_string(i + 1) + std::to_string(j + 1)) : "0");
                    lineU.push_back(i == j ? "1" : i < j ? ("U" + std::to_string(i + 1) + std::to_string(j + 1)) : "0");
                }
            }

            helpL.push_back(lineL);
            helpU.push_back(lineU);
        }
    }

    void buildYDoolittle() {
        std::string buffer{};
        Y[0] = b[0];
        std::cout << helpY[0] + "=" + out.to_string(b[0]) + "-0=" + out.to_string(b[0]) + "\n";

        for (int i = 1; i < n; i++) {
            std::string sum1{}, sum2{}, sum3{};
            int sum = 0;

            for (int j = 0; j < n; j++) {
                sum += L[i][j] * Y[j];
                sum1 += helpL[i][j] + "*" + helpY[j] + "+";
                sum2 += out.to_string(L[i][j]) + "*" + out.to_string(Y[j]) + "+";
                sum3 += out.to_string(L[i][j] * Y[j]) + "+";
            }

            sum1 = out.removeLastChar(sum1);
            sum2 = out.removeLastChar(sum2);
            sum3 = out.removeLastChar(sum3);

            Y[i] = b[i] - sum;

            std::cout << helpY[i] + "=" + out.to_string(b[i]) + "-(" + sum1 + ")";
            std::cout << "=" + out.to_string(b[i]) + "-(" + sum2 + ")";
            std::cout << "=" + out.to_string(b[i]) + "-(" + sum3 + ")";
            std::cout << "=" + out.to_string(Y[i]) + "\n";
        }
    }

    void buildYCrout() {
        std::string buffer{};
        Y[0] = b[0] / L[0][0];
        std::cout << helpY[0] + "=(" + out.to_string(b[0]) + "-0)/1=" + out.to_string(b[0]) + "\n";

        for (int i = 1; i < n; i++) {
            std::string sum1{}, sum2{}, sum3{};
            int sum = 0;

            for (int j = 0; j < n; j++) {
                sum += L[i][j] * Y[j];
                sum1 += helpL[i][j] + "*" + helpY[j] + "+";
                sum2 += out.to_string(L[i][j]) + "*" + out.to_string(Y[j]) + "+";
                sum3 += out.to_string(L[i][j] * Y[j]) + "+";
            }

            sum1 = out.removeLastChar(sum1);
            sum2 = out.removeLastChar(sum2);
            sum3 = out.removeLastChar(sum3);

            Y[i] = (b[i] - sum) / L[i][i];

            std::cout << helpY[i] + "=[" + out.to_string(b[i]) + "-(" + sum1 + ")]/" + helpL[i][i];
            std::cout << "=[" + out.to_string(b[i]) + "-(" + sum2 + ")]/" + out.to_string(L[i][i]);
            std::cout << "=[" + out.to_string(b[i]) + "-(" + sum3 + ")]/" + out.to_string(L[i][i]);
            std::cout << "=" + out.to_string(Y[i]) + "\n";
        }
    }

    void buildXDoolittle() {
        X[n - 1] = Y[n - 1] / U[n - 1][n - 1];

        std::cout << helpX[n - 1] + "=" + out.to_string(Y[n - 1]) + "/" + helpU[n - 1][n - 1];
        std::cout << "=" + out.to_string(Y[n - 1]) + "/" + out.to_string(U[n - 1][n - 1]);
        std::cout << "=" + out.to_string(X[n - 1]) + "\n";

        for (int i = n - 2; i >= 0; i--) {
            std::string sum1{}, sum2{};
            double sum = 0;

            for (int j = i + 1; j < n; j++) {
                sum += U[i][j] * X[j];
                sum1 += helpU[i][j] + "*" + helpX[j] + "+";
                sum2 += out.to_string(U[i][j] * X[j]) + "+";
            }

            sum1 = out.removeLastChar(sum1);
            sum2 = out.removeLastChar(sum2);

            X[i] = (Y[i] - sum) / U[i][i];

            std::cout << helpX[i] + "=(" + sum1 + ")/" + helpU[i][i];
            std::cout << "=(" + sum2 + ")/" + out.to_string(U[i][i]);
            std::cout << "=" + out.to_string(X[i]) + "\n";
        }
    }

    void buildXCrout() {
        X[n - 1] = Y[n - 1] / U[n - 1][n - 1];

        std::cout << helpX[n - 1] + "=" + out.to_string(Y[n - 1]) + "/" + helpU[n - 1][n - 1];
        std::cout << "=" + out.to_string(X[n - 1]) + "\n";

        for (int i = n - 2; i >= 0; i--) {
            std::string sum1{}, sum2{};
            double sum = 0;

            for (int j = i + 1; j < n; j++) {
                sum += U[i][j] * X[j];
                sum1 += helpU[i][j] + "*" + helpX[j] + "+";
                sum2 += out.to_string(U[i][j] * X[j]) + "+";
            }

            sum1 = out.removeLastChar(sum1);
            sum2 = out.removeLastChar(sum2);

            X[i] = (Y[i] - sum) / U[i][i];

            std::cout << helpX[i] + "=[" + out.to_string(Y[i]) + "-(" + sum1 + ")]/" + helpU[i][i];
            std::cout << "=[" + out.to_string(Y[i]) + "-(" + sum2 + ")]/" + out.to_string(U[i][i]);
            std::cout << "=" + out.to_string(X[i]) + "\n";
        }
    }

    void initMatrixes() {
        L.reserve(static_cast<unsigned long>(n));
        U.reserve(static_cast<unsigned long>(n));

        for (int i = 0; i < n; i++) {
            std::vector<double> line;
            line.assign(n, 0);

            L.push_back(line);
            U.push_back(line);
        }
    }

    void initVectors() {
        X.resize(static_cast<unsigned long>(n));
        Y.resize(static_cast<unsigned long>(n));

        buildHelpVector(helpY, "y");
        buildHelpVector(helpX, "x");
    }

    void buildHelpVector(std::vector<std::string> &vector, const std::string &ch) {
        for (int i = 0; i < n; i++) {
            vector.push_back(ch + out.to_string(i + 1));
        }
    }

    void run() {
        buildHelpLU();

        std::cout << "Dacă λkk = 1 => factorizare Crout \n" <<
                  "Dacă μkk = 1 => factorizare Doolittle \n" <<
                  "Dacă matricea A este nesingulară (det A!=0) și toți determinanții de colț sunt != 0, atunci există LU a.î A=L*U.\n" <<
                  "Dacă unul dintre minori este 0, atunci nu se poate aplica metoda LU.\n\n";
        out.print(helpL, helpU, "L and U");
        std::cout << "\nA*X=b" << "\n";
        std::cout << "L*U*X=b" << "\n";
        std::cout << "L*y=b => y" << "\n";
        std::cout << "U*x=y => x" << "\n\n";

        if (doolittle) {
            buildLUDoolittle();

            out.print(L, helpY, b, "*", "L*y=b => y");
            buildYDoolittle();
            std::cout << "Y = (";
            out.print(Y);
            std::cout << ")\n\n";

            out.print(U, helpX, Y, "*", "U*x=y => x");
            buildXDoolittle();
            std::cout << "X = (";
            out.print(X);
            std::cout << ")\n\n";
        } else {
            buildLUCrout();

            out.print(L, helpY, b, "*", "L*y=b => y");
            buildYCrout();
            std::cout << "Y = (";
            out.print(Y);
            std::cout << ")\n\n";

            out.print(U, helpX, Y, "*", "U*x=y => x");
            buildXCrout();
            std::cout << "X = (";
            out.print(X);
            std::cout << ")\n\n";
        }


    }

public:
    LuFactorization(const std::vector<std::vector<double>> &matrix, std::vector<double> solution) :
            matrix(matrix), b(std::move(solution)) {
        n = static_cast<int>(matrix.size());

        initVectors();
        initMatrixes();
    }

    void runDoolittle() {
        doolittle = true;
        run();
    }

    void runCrout() {
        doolittle = false;
        run();
    }
};

#endif //LU_LUFACTORIZATION_HPP
