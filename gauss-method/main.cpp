#include <iostream>
#include "Gauss.hpp"

int main() {
    Gauss gauss({{0, 1,  2,  12},
                 {3, -3, 1,  6},
                 {2, 8,  -2, 8}});

//    gauss.runWithSemiPivot();
    gauss.runWithTotalPivot();
    return 0;
}