#include <iostream>
#include <vector>


void lagrange(const std::vector<int> &x, const std::vector<int> &y, const char ch) {
    std::vector<std::string> l{};

    for (int i = 0; i < x.size(); i++) {
        std::string tmp{};
        for (int j = 0; j < x.size(); j++) {
            if (i != j) {
                tmp += "(" + std::string(1, ch) + "-" + std::to_string(x[i]) + ")/(" + std::to_string(x[i]) + "-" +
                       std::to_string(x[j]) + ")*";
            }
        }
        tmp = tmp.substr(0, tmp.size() - 1);

        l.push_back(tmp);
    }

    for (int i = 0; i < x.size(); i++) {
        std::cout << y[i] << "*" << l[i] << "\n";
    }
}

int main() {
    std::vector<int> x{1, 2, 3, 4, 5}, y{2, 5, 10, 17, 26};
    lagrange(x, y, 'x');

    return 0;
}